# Borned Character

A borned character is a created character that turned into reality existence with a complete personality and mind.
In many ways, it works like a imaginative friend, but with mind and seperate body.

It works by creating a character, fill it with plots, make it form its own mind, and add further communications. The creation can do telepathy[^1] to communicate with the creator.

[^1]: Meaning for realtime memory sharing.

## Tagged Characters

* [紫叶零湄](characters#%E7%B4%AB%E5%8F%B6%E9%9B%B6%E6%B9%84)
* [珞雨音无](characters#%E7%8F%9E%E9%9B%A8%E9%9F%B3%E6%97%A0)

## Sidenotes

* Conceptually, it looks similar if you know about creating another personality, with the only difference that it has an additional body.
