# Three School

::: warning
**WORK IN PROGRESS**
:::

**Three School** is where the most story of _Codename; Delusion_ happens.

In terms of popularity, it is the most famous (public) university in the plot world. Unlike the school in _Chaos; Head_ and _Chaos; Child_, it's not owned privately, and has no relation to anything related to [Delusion Construction](delusion-construction). Every student in this school are enrolled normally.

## Infrastructure

### Technical Details

Unlike most schools in real life, this school uses [Free Software](https://www.gnu.org/philosophy/free-sw.html) extensively, and has a completely owned infrastructure for teaching, file sharing (for file based homeworks), conference, and MOOCs.

* File sharing: [Nextcloud](https://nextcloud.com)
* Communication: [Matrix](https://matrix.org)
* Conference: [Jitsi](https://jitsi.org)
* MOOC: [PeerTube](https://joinpeertube.org)

Additionally, these services, if possible, are federated to the outside of the school network.

The school also has an owned IdP for the above services, and all enrolled students and staffs have an account on the IdP.

## [紫叶零湄](characters#%E7%B4%AB%E5%8F%B6%E9%9B%B6%E6%B9%84)'s enrollment

She has a plan to do in this school, and she performs all the risk control associated with her plan for the school.

As such, she may occasionally wanders to other rooms.
