# Codename; Delusion Characters

Characters planned for Codename; Delusion.

::: info TODO
Seperate them into pages.
:::

## 紫叶零湄

Born from delusion. Has a girl body with the age of 20.

Before she became existed into reality, she was the imaginative friend of [Kurakaki Inokoto](#kurakaki-inokoto).

[The avatar image](https://picrew.me/share?cd=X8zVhfYvVB) represents her own body. It was first composed by Kurakaki Inokoto using a online service and used for the avatar of her.

## 珞雨音无

::: info TODO
Complete her story.
:::

Born from delusion. Has a girl body with the age of 19. [珞雨泛雪](#%E7%8F%9E%E9%9B%A8%E6%B3%9B%E9%9B%AA)'s imaginative friend.

珞雨泛雪 unconsciously created her into reality, and then forgets about this, she still remembers such thing however.
Even that, she didn't bother to find her creator because her value her creator's independence.

## 珞雨泛雪

::: info TODO
Complete her story.
:::

Originally, she has an imaginative friend called [珞雨音无](#%E7%8F%9E%E9%9B%A8%E9%9F%B3%E6%97%A0), but since then forget about this.

## Kurakaki Inokoto

MtF which died.

[紫叶零湄](#%E7%B4%AB%E5%8F%B6%E9%9B%B6%E6%B9%84) know this name, but rather keeps this name inside her memory. The name is also irrelevant to the entire story.

## 羽心印音

::: info TODO
Complete her story.
:::

The actual protagonist of the story.
