# Delusion Construction

::: warning
**WORK IN PROGRESS**.
:::

Delusion Construction is the broad term that can be applied to the delusion power that may exist on some characters. the abilities may vary, but are divided into two broad categories: **physical** and **mental**.

## Physical abilities

* Item control: You can control objects without touching the target object.
* Creation: Create arbitrary objects at your own will. However for a good creation, that requires you having a great projection in your head.\
  If the object created is a character, that character is called [Borned Character](borned-character).

## Mental abilities

* Mind talk: Communicate without using physical sounds. Requires someone with such ability to establish the channel.\
  While such talk can use any spoken language, this channel can, additionally, communicate with _primitives_[^1] to transfer infomations quickly and efficiently.
* Body control: Control a character's body. the target can take the ownership of the body, but that would become more difficult.\
  Depending on the effect scope, it can be divided into two categories:
  * Partial: Only a part of the body can be controlled.
  * Full: The body can be fully controlled.
* Possession: Unlike full body control, during possession, the target can't take the ownership of the body, and may know the body's actions or don't know.
* Additionally, it's possible to disassociate your mind from your body, and inject your mind to someone else's body, living with the original character's mind in the same body.

[^1]: This _primitives_ term is similar to the primitives instructions for a CPU, and high level programming languages are like our spoken languages.
