import { defineConfig } from 'vitepress'
import markdownItFootnote from 'markdown-it-footnote'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "@ShadowRZ's Codex",
  description: "My plots, and everything.",
  base: '/codex/',
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Home', link: '/' },
      {
        text: 'Projects',
        items: [
          { text: 'Codename; Delusion', link: '/codename-delusion/' },
          { text: 'Hugo Theme Nexmoe', link: '/hugo-theme-nexmoe/' }
        ],
      },
      {
        text: 'Others',
        items: [
          { text: 'Components', link: '/components/' },
          { text: 'Examples', link: '/examples/markdown-examples' }
        ],
      },
    ],

    sidebar: {
      '/codename-delusion/': [
        {
          text: 'Codename; Delusion',
          link: '/codename-delusion/',
          items: [
            {
              text: 'Concepts',
              items: [
                { text: 'Delusion Construction', link: '/codename-delusion/delusion-construction' },
                { text: 'Borned Character', link: '/codename-delusion/borned-character' },
              ],
            },
            { text: 'Characters', link: '/codename-delusion/characters' },
            { text: 'Three School', link: '/codename-delusion/three-school' },
          ]
        },
      ],
      '/hugo-theme-nexmoe/': [
        {
          text: 'Hugo Theme Nexmoe',
          link: '/hugo-theme-nexmoe/',
          items: [
            { text: 'Quick Start', link: '/hugo-theme-nexmoe/quickstart' },
          ],
        },
        {
          text: 'Configuration',
          items: [
            { text: 'Theme Config', link: '/hugo-theme-nexmoe/configuration' },
            { text: 'Custom Layouts', link: '/hugo-theme-nexmoe/custom-layouts' },
            { text: 'Custom Styles', link: '/hugo-theme-nexmoe/custom-styles' },
          ],
        },
        {
          text: 'Pages & Posts',
          items: [
            { text: 'Front Matter', link: '/hugo-theme-nexmoe/front-matter' },
            { text: 'Links Shortcode', link: '/hugo-theme-nexmoe/links-shortcode' },
          ],
        },
      ],
      '/examples/': [
        {
          text: 'Examples',
          items: [
            { text: 'Markdown Examples', link: '/examples/markdown-examples' },
            { text: 'Runtime API Examples', link: '/examples/api-examples' }
          ]
        },
      ],
      '/components/': [
        {
          text: 'Components',
          items: [
            { text: 'Timeline', link: '/components/timeline' },
          ]
        },
      ]
    },
    outline: 'deep',
    aside: false,
    search: {
      provider: 'local'
    },
    externalLinkIcon: true,
  },
  outDir: "../public",
  head: [
    [
      "link",
      {
        rel: "apple-touch-icon",
        href: "/codex/apple-touch-icon.png",
      },
    ],
    [
      "link",
      { rel: "icon", type: "image/png", sizes: "32x32", href: "/codex/favicon-32x32.png" },
    ],
    [
      "link",
      { rel: "icon", type: "image/png", sizes: "16x16", href: "/codex/favicon-16x16.png" },
    ],
    ["link", { rel: "manifest", href: "/codex/site.webmanifest" }],
    ["link", { rel: "shortcut icon", href: "/codex/favicon.ico" }],
    ["meta", { name: "theme-color", content: "#9d174d" }],
  ],
  lastUpdated: true,
  markdown: {
    config: (md) => {
      md.use(markdownItFootnote)
    },
  },
  transformHead: ({ assets }) => {
    // adjust the regex accordingly to match your font
    const files = assets.filter(file => file.match(/jost-latin-[0-9]00.*\.woff2$/))
    return files.map((file) => [
      'link',
      {
        rel: 'preload',
        href: file,
        as: 'font',
        type: 'font/woff2',
        crossorigin: ''
      }
    ])
  },
})
