# Theme Config

`hugo.toml`[^1] is the central config of your Hugo site including Nexmoe theme config.

[^1]: [Since v0.110.0](https://gohugo.io/getting-started/configuration/#hugotoml-vs-configtoml), was `config.toml` before.

::: info
You can also use YAML / JSON to write your Hugo config:

* YAML: `hugo.yaml`
* JSON: `hugo.json` (Rare)

Also see [Hugo documentation](https://gohugo.io/getting-started/configuration/).
:::

::: info
In the following sections, all configuration fields will be presented in the three formats: TOML, YAML and JSON.
::: code-group
```toml [TOML]
theme = "nexmoe"
[author]
  name = "Example"
```

```yaml [YAML]
theme: nexmoe
author:
  name: Example
```

```JSON [JSON]
{
  "theme": "nexmoe",
  "author": {
    "name": "Example"
  }
}
```
:::

## All configuration options

This is a list of all configuration options that are relevant to the theme:

::: code-group
<<< @/../files/nexmoe/hugo.toml [TOML]
<<< @/../files/nexmoe/hugo.yaml [YAML]
<<< @/../files/nexmoe/hugo.json [JSON]
:::

### `avatar`

The avatar for the site.

::: code-group
```toml [TOML]
[params]
avatar = "https://cdn.jsdelivr.net/gh/nexmoe/nexmoe.github.io@latest/images/avatar.png"
```

```yaml [YAML]
params:
  avatar: https://cdn.jsdelivr.net/gh/nexmoe/nexmoe.github.io@latest/images/avatar.png
```

```json [JSON]
{
  "params": {
    "avatar": "https://cdn.jsdelivr.net/gh/nexmoe/nexmoe.github.io@latest/images/avatar.png"
  }
}
```
:::

### `background.path`

The background and the default header picture for the article (if a post didn't provide one).

::: code-group
```toml [TOML]
[params.background]
path = "https://i.dawnlab.me/b5a7e61560facdd5b8aa9683fce147d7.png"
```

```yaml [YAML]
params:
  background:
    path: https://i.dawnlab.me/b5a7e61560facdd5b8aa9683fce147d7.png
```

```json [JSON]
{
  "params": {
    "background": {
      "path": "https://i.dawnlab.me/b5a7e61560facdd5b8aa9683fce147d7.png"
    }
  }
}
```
:::

### `function`

* `globalToc`: Enable global TOC.
* `wordCount`: Enable word counting.
* `copyCode`: Enable the button to copy code.

::: code-group
```toml [TOML]
[params.function]
globalToc = true
wordCount = false
copyCode = true
```

```yaml [YAML]
params:
  function:
    globalToc: true
    wordCount: false
    copyCode: true
```

```json [JSON]
{
  "params": {
    "function": {
      "globalToc": true,
      "wordCount": false,
      "copyCode": true
    }
  }
}
```
:::

### Menu

Refer to [Hugo docs](https://gohugo.io/content-management/menus/) for how to configure your site menu.

A top level menu with ID `main` would be used as the navigaion menu.

Accepted `params`:

* `icon`: A Iconify icon name for the icon. Refer to [Iconify Icon Library](https://icon-sets.iconify.design) for icons.

### Widgets

The following widgets are avaliable in this port:

#### Social

Specify a dictionary pairs in `options.social`, the key would be the social link name.

The value should be specified in a array containing these four elements:

1. The link of the social link.
2. A Iconify icon name for the icon shown. Refer to [Iconify Icon Library](https://icon-sets.iconify.design) for icons.
3. A vaild [CSS color definition](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value) as the foreground icon color.
4. A vaild [CSS color definition](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value) as the background icon color.

::: warning
Duotone icons may not work properly with foreground colors.
:::

::: code-group
```toml [TOML]
[[params.widgets]]
name = "social"
enable = true

[params.widgets.options.social]
GitHub = [
  "https://github.com",
  "ph:github-logo",
  "rgb(25, 23, 23)",
  "rgba(25, 23, 23, .1)"
]
Twitter = [
  "https://twitter.com",
  "ph:twitter-logo",
  "rgb(59, 151, 239)",
  "rgba(59, 151, 239, .1)"
]
```

```yaml [YAML]
params:
  widgets:
    - name: social
      enable: true
      options:
        social:
          GitHub:
            - https://github.com
            - ph:github-logo
            - rgb(25, 23, 23)
            - rgba(25, 23, 23, .1)
          Twitter:
            - https://twitter.com
            - ph:twitter-logo
            - rgb(59, 151, 239)
            - rgba(59, 151, 239, .1)
```

```json [JSON]
{
  "params": {
    "widgets": [
      {
        "name": "social",
        "enable": true,
        "options": {
          "social": {
            "GitHub": [
              "https://github.com",
              "ph:github-logo",
              "rgb(25, 23, 23)",
              "rgba(25, 23, 23, .1)"
            ],
            "Twitter": [
              "https://twitter.com",
              "ph:twitter-logo",
              "rgb(59, 151, 239)",
              "rgba(59, 151, 239, .1)"
            ]
          }
        }
      }
    ]
  }
}
```
:::

#### Categories

Renders all avaliable categories.

::: code-group
```toml [TOML]
[[params.widgets]]
name = "category"
enable = true
```

```yaml [YAML]
params:
  widgets:
    - name: category
      enable: true
```

```json [JSON]
{
  "params": {
    "widgets": [
      {
        "name": "category",
        "enable": true
      }
    ]
  }
}
```
:::

#### Recent posts

Renders recent 5 posts.

::: code-group
```toml [TOML]
[[params.widgets]]
name = "recent_posts"
enable = true
```

```yaml [YAML]
params:
  widgets:
    - name: recent_posts
      enable: true
```

```json [JSON]
{
  "params": {
    "widgets": [
      {
        "name": "recent_posts",
        "enable": true
      }
    ]
  }
}
```
:::

#### Hitokoto

Dynamically renders a random quote, backed by [hitokoto.cn](https://hitokoto.cn).

Fields in `options.widgetHitokoto`:

* `loading_placeholder`: The placeholder shown while waiting for random quote.
* `loading_error_placeholder`: The placeholder shown if loading a random quote failed.
* `category`: Optional. Specify a category character defined in [Hitokoto Docs](https://developer.hitokoto.cn/sentence/#%E5%8F%A5%E5%AD%90%E7%B1%BB%E5%9E%8B-%E5%8F%82%E6%95%B0).

::: code-group
```toml [TOML]
[[params.widgets]]
name = "hitokoto"
enable = false

[params.widgets.options.widgetHitokoto]
loading_placeholder = "Loading..."
loading_error_placeholder = "Loading Failed. :("
category = ""
```

```yaml [YAML]
params:
  widgets:
    - name: hitokoto
      enable: false
      options:
        widgetHitokoto:
          loading_placeholder: Loading...
          loading_error_placeholder: Loading Failed. :(
          category: ""
```

```json [JSON]
{
  "params": {
    "widgets": [
      {
        "name": "hitokoto",
        "enable": false,
        "options": {
          "widgetHitokoto": {
            "loading_placeholder": "Loading...",
            "loading_error_placeholder": "Loading Failed. :(",
            "category": ""
          }
        }
      }
    ]
  }
}
```
:::
