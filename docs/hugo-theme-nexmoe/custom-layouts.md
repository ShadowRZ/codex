# Custom layouts

The following partials are predefined:

* `layouts/partials/customize/head.html`: Added to `<head>` sections.
* `layouts/partials/customize/footer.html`: Added to the end of the page.
* `layouts/partials/customize/sidebar.html`: Added to the sidebar.
* `layouts/partials/customize/comment.html`: Specify any HTML / CSS / JavaScript to power the page comments.
