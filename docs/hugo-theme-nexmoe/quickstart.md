# Quick Start

## Requirements

Make sure the Hugo you have is the **extended** version, as this theme uses SCSS.

To check the Hugo version you have[^1]:

```console
$ hugo version
hugo v0.119.0+extended linux/amd64 BuildDate=unknown VendorInfo=nixpkgs
```

[^1]: Taken from the [Nixpkgs](https://github.com/NixOS/nixpkgs) build.

Notice the version suffix has `+extended`, which means you have the correct Hugo version.

## Use the theme

Assuming you already has a Hugo site:

### Git

Clone the theme into the themes folder:

```console
$ git clone https://github.com/ShadowRZ/hugo-theme-nexmoe themes/nexmoe
```

Or as a submodule:

```console
$ git submodule add https://github.com/ShadowRZ/hugo-theme-nexmoe themes/nexmoe
```
