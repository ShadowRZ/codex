# Hugo Theme Nexmoe

This is a Hugo theme ported from [the Hexo version](https://github.com/theme-nexmoe/hexo-theme-nexmoe).

::: warning
**DOCUMENTATION WORK IN PROGRESS**
:::

## Modifications

* Use [Iconify](https://iconify.design) instead of pre-provided Nexmoe icon fonts.
