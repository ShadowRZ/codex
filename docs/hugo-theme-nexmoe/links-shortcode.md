# Links Shortcode

Links shortcode are supported.

::: warning
It's possible to use JSON/YAML to write this shortcode **but not TOML** due to its inability to represent top level object array.
:::

## Fixed

The order won't change across rebuilds.

::: code-group

``` [JSON]
{{< links >}}
[
  {
    "title": "Yorusaka Miyabi",
    "link": "https://shadowrz.github.io/blog/",
    "img": "https://www.libravatar.org/avatar/c08fdd039b5c7a2da68b65c046356120d55f0704d876180e74bba90a54462ec3?s=400"
  },
  {
    "title": "Hugo",
    "link": "https://gohugo.io",
    "img": "https://gohugo.io/android-chrome-256x256.png",
    "des": "The world's fastest framework for building websites."
  }
]
{{< /links >}}
```

``` [YAML]
{{< links >}}
- title: Yorusaka Miyabi
  link: https://shadowrz.github.io/blog/
  img: https://www.libravatar.org/avatar/c08fdd039b5c7a2da68b65c046356120d55f0704d876180e74bba90a54462ec3?s=400
- title: Hugo
  link: https://gohugo.io
  img: https://gohugo.io/android-chrome-256x256.png
  des: The world's fastest framework for building websites.
{{< /links >}}
```

:::

## Shuffled

The order will change across rebuilds.

::: code-group

``` [JSON]
{{< links shuffle >}}
[
  {
    "title": "喵's StackHarbor",
    "link": "https://sh.alynx.one",
    "img": "https://sh.alynx.one/images/Mikoto_Karon_White.webp"
  },
  {
    "title": "依云's Blog",
    "link": "https://blog.lilydjwg.me",
    "img": "https://blog.lilydjwg.me/user_files/lilydjwg/config/avatar.png"
  },
  {
    "title": "Vifly 的博客",
    "link": "https://viflythink.com",
    "img": "https://viflythink.com/img/avatar.png"
  },
  {
    "title": "Sukka's Blog",
    "link": "https://blog.skk.moe",
    "img": "https://cdn.jsdelivr.net/npm/skx@0.2.3/avatar/96x96.png"
  }
]
{{< /links >}}
```

``` [YAML]
{{< links shuffle >}}
- title: 喵's StackHarbor
  link: https://sh.alynx.one
  img: https://sh.alynx.one/images/Mikoto_Karon_White.webp
- title: 依云's Blog
  link: https://blog.lilydjwg.me
  img: https://blog.lilydjwg.me/user_files/lilydjwg/config/avatar.png
- title: Vifly 的博客
  link: https://viflythink.com
  img: https://viflythink.com/img/avatar.png
- title: Sukka's Blog
  link: https://blog.skk.moe
  img: https://cdn.jsdelivr.net/npm/skx@0.2.3/avatar/96x96.png
{{< /links >}}
```

:::
