# Front Matter

In addition to the [predefined front matter variables](https://gohugo.io/content-management/front-matter/#predefined), Nexmoe theme reserves these additional fields:

| Field | Description | Default |
|------ | ----------- | ------- |
| `cover` | Specify a page-specific cover. | Defined in [theme config](configuration#background-path) |
| `coverWidth` | The width of the cover. | 1600 |
| `coverHeight` | The height of the cover. | 900 |
| `reprint` | Specify the article is reposted. | false |
