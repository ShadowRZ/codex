---
prev: false
next: false
---

# Introduction

As the subtitle said, "Content ranges from my plots, my characters, to everything from reality to virtual".

You can consider this site as my personal wiki, although it can be outdated, and most of them works like a draft.

The key words "**MUST**", "**MUST NOT**", "**REQUIRED**", "**SHALL**", "**SHALL NOT**",
"**SHOULD**", "**SHOULD NOT**", "**RECOMMENDED**",  "**MAY**", and
"**OPTIONAL**", if exists, are to be interpreted as described in
[**RFC 2119**](https://datatracker.ietf.org/doc/html/rfc2119).

::: warning
This codex is a **WORK IN PROGRESS**. Expect everything missing.
:::

::: warning
Sidebar has been removed from toplevel pages, use the top navigation bar to find projects.
:::
