# Component | Timeline

A component of timeline view.

Based on how [Stellar](https://xaoxuu.com/wiki/stellar/) implments this.

## Vanilla

[Open in Codepen](https://codepen.io/ShadowRZ/pen/dyQbxzd)

::: code-group

```html [HTML]
<div class="timeline">
  <section class="timeline-item">
    <div class="timeline-header">
      <p>Header Title</p>
    </div>
    <div class="timeline-content">
      This is some timeline content.
    </div>
  </section>
  <section class="timeline-item">
    <div class="timeline-header">
      <p>Header Title #2</p>
    </div>
    <div class="timeline-content">
      This is another timeline content.
    </div>
  </section>
</div>
```

```css [CSS]
/* Optional: Process this CSS with Autoprefixer */
.timeline {
  position: relative;
  padding-left: 16px;
  margin-top: 0px;
  --timeline-width: 4px;
}

.timeline::before {
  position: absolute;
  content: "";
  background: #f4f4f5;
  width: var(--timeline-width);
  left: 0;
  right: 0;
  bottom: 0;
  top: 0.5rem;
  z-index: 0;
  border-radius: 4px;
}

.timeline-item {
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  margin-top: 0.5rem;
}

.timeline-item:first-child {
  margin-top: 0;
}

.timeline-item .timeline-header {
  display: flex;
  align-items: center;
  position: relative;
}

.timeline-item .timeline-header p {
  margin: 0px;
  line-height: 1;
}

.timeline-item .timeline-header::before {
  position: absolute;
  content: "";
  background: #a1a1aa;
  width: 8px;
  height: 8px;
  left: -18px;
  right: 0;
  top: calc(50% - 0.5 * 8px);
  bottom: -2px;
  z-index: 0;
  border-radius: 4px;
}

.timeline-item:hover .timeline-header::before {
  background: #fb923c;
}

/* Customize content. */
.timeline-content {
  padding: 0.5em;
  margin: 0.25rem;
  border-radius: 8px;
  box-shadow: 0 1px 3px 0 rgb(0 0 0 / 0.1), 0 1px 2px -1px rgb(0 0 0 / 0.1);
  transition: box-shadow 0.5s;
}

.timeline-item:hover .timeline-content {
  box-shadow: 0 4px 6px -1px rgb(0 0 0 / 0.1), 0 2px 4px -2px rgb(0 0 0 / 0.1);
  transition: box-shadow 0.5s;
}
```

:::

## Vue

<script setup>
import TimelineContent from './TimelineContent.vue';
import TimelineSection from './TimelineSection.vue';
</script>

<TimelineContent>
  <TimelineSection title="Item 1">
    This is an item
  </TimelineSection>
  <TimelineSection title="Item 2">
    This is another item
  </TimelineSection>
</TimelineContent>

::: code-group

```vue [Usage]
<script setup>
import TimelineContent from 'path/to/TimelineContent.vue';
import TimelineSection from 'path/to/TimelineSection.vue';
</script>

<template>
  <TimelineContent>
    <TimelineSection title="Item 1">
      This is an item
    </TimelineSection>
    <TimelineSection title="Item 2">
      This is another item
    </TimelineSection>
  </TimelineContent>
</template>
```

<<< @/components/TimelineContent.vue [TimelineContent.vue]

<<< @/components/TimelineSection.vue [TimelineSection.vue]

:::
