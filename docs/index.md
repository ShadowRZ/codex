---
# https://vitepress.dev/reference/default-theme-home-page
layout: home
titleTemplate: false

hero:
  name: "@ShadowRZ's Codex"
  text: "My plots, and everything."
  tagline: "[WIP] Content ranges from my plots, my characters, to everything from reality to virtual. (?)"
  actions:
    - theme: brand
      text: What's this?
      link: /introduction
features:
  - title: Codename; Delusion
    details: Retake of my original plot revolving around characters born from delusion.
    link: /codename-delusion/
    linkText: Codex Index
  - title: Hugo Theme Nexmoe
    details: Hugo port of the Hexo Nexmoe theme.
    link: /hugo-theme-nexmoe/
    linkText: View Docs
---
